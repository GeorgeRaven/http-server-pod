TAG=debug/pod
CONTAINER_NAME=http-debug-pod
EXPORT_DIR=public

.PHONY: all
all: build test run

.PHONY: build
build:
	podman build -t ${TAG} -f Dockerfile .

.PHONY: test
test:
	cd server && go test -short $(go list ./... | grep -v /vendor/)

.PHONY: run
run: build
	xdg-open "http://127.0.0.1:8083" &
	podman run -p 127.0.0.1:8083:8080 -it ${TAG}

.PHONY: cp
cp: build
	podman rm -f ${CONTAINER_NAME}
	podman run -d --name ${CONTAINER_NAME} --entrypoint /bin/sleep ${TAG} 20
	podman cp ${CONTAINER_NAME}:/app/static ${EXPORT_DIR}
	podman rm -f ${CONTAINER_NAME}
